#!/bin/bash
cat /etc/*-release |& tee -a log.txt
lsb_release -rs |& tee -a  log.txt
sudo dnf update -y |& tee -a  log.txt 
sudo dnf config-manager --set-disabled anydesk |& tee -a log.txt
sudo dnf upgrade --refresh -y |& tee -a  log.txt
sudo dnf install dnf-plugin-system-upgrade -y |& tee -a  log.txt
sudo dnf system-upgrade download --releasever=40 -y |& tee -a  log.txt
sudo uname -r
echo 'dnf system-upgrade reboot'
echo 'cat /etc/*-release'
echo 'lsb_release -rs'
